(function () {

    var express = require("express")
    var bodyParser = require("body-parser")
    var cluster = require("cluster")

    // Attaching event handlers to cluster    
    cluster.on("online", function (worker) { console.log("Worker started with pid:- %s", worker.process.pid) })
    cluster.on("exit", function (worker, code, signal) { console.log("Worker(%s) exited with code:- %d and signal:- %f", worker.process.pid, code, signal) })
    cluster.on("error", function (worker, error) { console.log("Worker(%s) having error:- %d", worker.process.pid, error.toString()) })

    if (cluster.isMaster) {
        var workerCount = require("os").cpus().length
        for (var i = 0; i < workerCount; i++) {
            cluster.fork()
        }
    } else {
        startServer()
    }

    function startServer() {
        var app = express()
        var router = require("./routes.js")

        // Configuring app
        app.use(bodyParser.json())
        app.use(bodyParser.urlencoded({extended: true}))
        app.use(express.static("../client"))
        
        app.use("/", router)

        app.listen(8080)        
    }

})()
