(function () {

    var express = require("express")
    var fs = require("fs")
    var vidStreamer = require("vid-streamer")

    var router = express.Router()
    
    vidStreamer.settings({rootFolder: "./videos/"})
    router.get("/videos/:path", vidStreamer)

    router.get("/browse/:path", function (req, res) {
        var path = req.params.path.trim()
        // console.log(path)
        var folPath = ["./videos/", path].join("")
        var files = fs.readdirSync(folPath)
        var resp = files.map(function (file) {
            var fPath = ["./videos/", path, file].join("/")
            return {
                name: file,
                isFile: fs.statSync(fPath).isFile()
            }
        })
        res.json(resp)
    })

    module.exports = router    

})()
