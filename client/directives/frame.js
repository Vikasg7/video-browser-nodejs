(function () {

   angular
      .module("frame", [])
      .directive("frame", function ($compile) {
         return {
            restrict: "A",
            link: function (scope, ele, attrs) {
               var template = '<link rel="stylesheet" href="./css/iframe.css"><video autoplay controls><source video-src="./videos/{{vpc.filename}}"></video>'
               var compiled = $compile(template)(scope)
               var body = angular.element(ele[0].contentDocument.body)
               body.append(compiled)
            }
         }
      })

})()