(function () {

    angular
        .module("video-src", [])
        .directive("videoSrc", ["$compile", "$interpolate", function ($compile, $interpolate) {
            return {
                restrict: "A",
                link: function (scope, element, attrs) {
                    var src = $interpolate(attrs.videoSrc)(scope)
                    element.removeAttr("video-src")
                    element.attr("src", src)
                    $compile(element)(scope)
                }
            }
        }])

})()
