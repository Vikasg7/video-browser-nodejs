(function () {
   
    angular
        .module("include-me", [])
        .directive("includeMe", function () {
            return {
                restrict: "AE",
                templateUrl: function (ele, attrs) {
                    return attrs.includeMe
                }
            }
        })

})()
