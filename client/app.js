(function () {

    angular
        .module("App", ["ui.router", "include-me", "video-src", "frame", "app.controllers"])
        .config(["$stateProvider", "$urlRouterProvider", function (stateProvider, urlRouterProvider) {
            
            // urlRouterProvider.otherwise("/")
            
            stateProvider
                .state("explore", {
                    url: "/explore?path",
                    templateUrl: "./views/explorer.html",
                    controller: "explorerCtrl as ec"
                })
                    .state("explore.play", {
                        url: "/play?filename",
                        templateUrl: "./views/videoPlayer.html",
                        controller: "videoPlayerCtrl as vpc"                               
                    })
        }])

})()
