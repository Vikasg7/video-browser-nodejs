(function () {

    angular
        .module("app.controllers") // Initialising controllers module
        .controller("explorerCtrl", ["$scope", "$http", "$state", "$stateParams", function ($scope, $http, $state, $stateParams) {
        
            var ec = this
            var delim = "/"

            ec.folderElements = []

            ec.ac = $scope.$parent.ac

            ec.previousFolders = ec.ac.previousFolders
            
            // Both currentFol variables should refer to same place
            ec.currentFol = ec.ac.currentFol = $stateParams.path

            function makeUrl() {
                var folPath = ec.previousFolders.slice(0)
                folPath.push(ec.currentFol)
                folPath = encodeURIComponent(folPath.join(delim).replace(/\/{2,}/g, "/"))
                var url = ["/browse", folPath].join(delim)
                return url
            }

            function browse() {             
                ec.loading = true  // Loader on
                url = makeUrl()
                $http.get(url).then(function (data) {
                    ec.folderElements = data.data
                    ec.loading = false
                })                
            }

            browse()

        }])

})()
