(function () {

    angular
        .module("app.controllers", [])
        .controller("appCtrl", ["$scope", "$state", function ($scope, $state) {
            var ac = this
            var delim = "/"
            
            ac.previousFolders = localStorage.previousFolders ? JSON.parse(localStorage.previousFolders) : []
            
            ac.currentFol = localStorage.currentFol ? JSON.parse(localStorage.currentFol) : " ./"
            
            ac.openFol = function (index) {
                ac.currentFol = ac.previousFolders[index]
                ac.previousFolders = ac.previousFolders.slice(0, index)
                localStorage.currentFol = JSON.stringify(ac.currentFol)
                localStorage.previousFolders = JSON.stringify(ac.previousFolders)
                $state.go("explore", {path: ac.currentFol})
            }

            ac.openNext = function (element) {
                ac.previousFolders.push(ac.currentFol)
                localStorage.previousFolders = JSON.stringify(ac.previousFolders)
                ac.currentFol = element.name
                localStorage.currentFol = JSON.stringify(ac.currentFol)
                $state.go("explore", {path: ac.currentFol})
            }

            ac.openPrevious = function () {
                if (ac.previousFolders.length) {
                    ac.currentFol = ac.previousFolders.pop()
                    localStorage.currentFol = JSON.stringify(ac.currentFol)
                    localStorage.previousFolders = JSON.stringify(ac.previousFolders)
                    $state.go("explore", {path: ac.currentFol})                    
                }
            }

            ac.play = function (element) {
                var folPath = ac.previousFolders.slice(0)
                folPath.push(ac.currentFol)
                folPath.push(element.name)
                var filename = encodeURIComponent(folPath.join(delim).replace(/\/{2,}/g, "/"))
                $state.go("explore.play", {filename: filename})
            }

            $state.go("explore", {path: ac.currentFol})
        }])

})()